const mongoose = require("mongoose");
const bcrypt =  require('bcrypt');
const Schema = mongoose.Schema;
let UsersSchema = new Schema({
  name: {
    type: String,
    required: true,
    max: 100,
  },
  email: {
    type: String,
    required: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  birthDate: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    required: true
  },
});

UsersSchema.pre('save', async function(next) {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;
    next();
});

module.exports = mongoose.model("Users", UsersSchema);
