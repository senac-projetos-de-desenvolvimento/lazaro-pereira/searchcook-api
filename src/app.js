const express = require('express');
const server = express();
const user = require('./routes/users.route');
const recipe = require('./routes/recipes.route');
const connectDB = require('./db/connect');
require('dotenv').config();
var cors = require('cors');

const port = 3001;

const start = async() => {
    try {
        await connectDB(process.env.MONGO_URL);
        server.listen(port, console.log(`Server is running on port ${port}`));
    } catch (error) {
        console.log(error);
    }
}

server.use(cors());
server.use(express.json());
server.use('/user', user);
server.use('/recipe', recipe);

start();

// role usuario

// tabela like

// tabela - ingrediente: separar quantidade e medida (colocar na tabela pivo)
